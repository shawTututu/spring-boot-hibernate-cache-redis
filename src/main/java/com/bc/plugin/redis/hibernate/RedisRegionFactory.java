package com.bc.plugin.redis.hibernate;

import java.util.Properties;

import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.CacheException;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;

import com.bc.plugin.redis.hibernate.config.RedisCacheFactoryAutoConfiguration;
import com.bc.plugin.redis.hibernate.help.RedisTimestamp;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RedisRegionFactory extends AbstractRedisRegionFactory {
	private static final long serialVersionUID = 1L;

	public RedisRegionFactory() {
	}

	RedisAtomicLong redisTimestamp;


	@Override
	public long nextTimestamp() {
		return RedisTimestamp.nextTimestamp(redisTimestamp);
	}

	@Override
	public void start(SessionFactoryOptions settings, Properties properties) throws CacheException {
		log.debug("RedisRegionFactory is starting... options={}, properties={}", options, properties);
		redisTimestamp = new RedisAtomicLong("regionFactory:system:nanoTime",
				RedisCacheFactoryAutoConfiguration.TIMESTAMP_REDIS_TEMPLATE);
	}

	@Override
	public void stop() {

	}
}
