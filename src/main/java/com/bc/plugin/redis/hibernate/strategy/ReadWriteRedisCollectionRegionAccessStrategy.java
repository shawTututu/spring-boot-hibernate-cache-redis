package com.bc.plugin.redis.hibernate.strategy;

import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.internal.DefaultCacheKeysFactory;
import org.hibernate.cache.spi.CollectionRegion;
import org.hibernate.cache.spi.access.CollectionRegionAccessStrategy;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.persister.collection.CollectionPersister;

import com.bc.plugin.redis.hibernate.regions.RedisCollectionRegion;


public class ReadWriteRedisCollectionRegionAccessStrategy
    extends AbstractReadWriteRedisAccessStrategy<RedisCollectionRegion>
    implements CollectionRegionAccessStrategy {

  public ReadWriteRedisCollectionRegionAccessStrategy(RedisCollectionRegion region,
                                                      SessionFactoryOptions options) {
    super(region, options);
  }

  @Override
  public Object generateCacheKey(Object id,
                                 CollectionPersister persister,
                                 SessionFactoryImplementor factory,
                                 String tenantIdentifier) {
    return DefaultCacheKeysFactory.INSTANCE.createCollectionKey(id, persister, factory, tenantIdentifier);
  }

  @Override
  public Object getCacheKeyId(Object cacheKey) {
    return DefaultCacheKeysFactory.INSTANCE.getCollectionId(cacheKey);
  }

  @Override
  public CollectionRegion getRegion() {
    return region;
  }
}
