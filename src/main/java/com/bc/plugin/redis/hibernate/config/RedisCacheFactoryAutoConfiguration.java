package com.bc.plugin.redis.hibernate.config;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

@Component
public class RedisCacheFactoryAutoConfiguration implements BeanPostProcessor, InitializingBean {
	
	public static RedisTemplate<String, Object> REDIS_TEMPLATE;
	public static RedisTemplate<String, Long> TIMESTAMP_REDIS_TEMPLATE;
	
	@Autowired
	RedisConnectionFactory redisConnectionFactory;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		REDIS_TEMPLATE = new RedisTemplate<>();
		REDIS_TEMPLATE.setKeySerializer(new StringRedisSerializer());
		REDIS_TEMPLATE.setConnectionFactory(redisConnectionFactory);
		REDIS_TEMPLATE.afterPropertiesSet();
		TIMESTAMP_REDIS_TEMPLATE = new RedisTemplate<>();
		TIMESTAMP_REDIS_TEMPLATE.setConnectionFactory(redisConnectionFactory);
		TIMESTAMP_REDIS_TEMPLATE.setKeySerializer(new StringRedisSerializer());
		TIMESTAMP_REDIS_TEMPLATE.setValueSerializer(new GenericToStringSerializer<Long>(Long.class));
		TIMESTAMP_REDIS_TEMPLATE.afterPropertiesSet();
	}

}
