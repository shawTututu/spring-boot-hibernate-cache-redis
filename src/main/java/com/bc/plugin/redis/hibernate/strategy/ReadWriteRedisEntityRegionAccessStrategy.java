
package com.bc.plugin.redis.hibernate.strategy;

import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.internal.DefaultCacheKeysFactory;
import org.hibernate.cache.spi.EntityRegion;
import org.hibernate.cache.spi.access.EntityRegionAccessStrategy;
import org.hibernate.cache.spi.access.SoftLock;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.persister.entity.EntityPersister;

import com.bc.plugin.redis.hibernate.regions.RedisEntityRegion;

public class ReadWriteRedisEntityRegionAccessStrategy extends AbstractReadWriteRedisAccessStrategy<RedisEntityRegion>
		implements EntityRegionAccessStrategy {

	public ReadWriteRedisEntityRegionAccessStrategy(RedisEntityRegion region, SessionFactoryOptions options) {
		super(region, options);
	}

	@Override
	public Object generateCacheKey(Object id, EntityPersister persister, SessionFactoryImplementor factory,
			String tenantIdentifier) {
		return DefaultCacheKeysFactory.INSTANCE.createEntityKey(id, persister, factory, tenantIdentifier);
	}

	@Override
	public Object getCacheKeyId(Object cacheKey) {
		return DefaultCacheKeysFactory.INSTANCE.getEntityId(cacheKey);
	}

	@Override
	public EntityRegion getRegion() {
		return region;
	}

	@Override
	public boolean insert(SharedSessionContractImplementor session, Object key, Object value, Object version) {
		region.put(key, value);
		return true;
	}

	@Override
	public boolean afterInsert(SharedSessionContractImplementor session, Object key, Object value, Object version) {
		region.put(key, value);
		return true;
	}

	@Override
	public boolean update(SharedSessionContractImplementor session, Object key, Object value, Object currentVersion,
			Object previousVersion) {
		region.put(key, value);
		return true;
	}

	@Override
	public boolean afterUpdate(SharedSessionContractImplementor session, Object key, Object value,
			Object currentVersion, Object previousVersion, SoftLock lock) {
		region.put(key, value);
		return true;
	}
}
