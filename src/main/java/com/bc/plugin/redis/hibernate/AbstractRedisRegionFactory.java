package com.bc.plugin.redis.hibernate;

import java.util.Properties;

import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.CacheDataDescription;
import org.hibernate.cache.spi.CollectionRegion;
import org.hibernate.cache.spi.EntityRegion;
import org.hibernate.cache.spi.NaturalIdRegion;
import org.hibernate.cache.spi.QueryResultsRegion;
import org.hibernate.cache.spi.RegionFactory;
import org.hibernate.cache.spi.TimestampsRegion;
import org.hibernate.cache.spi.access.AccessType;

import com.bc.plugin.redis.hibernate.regions.RedisCollectionRegion;
import com.bc.plugin.redis.hibernate.regions.RedisEntityRegion;
import com.bc.plugin.redis.hibernate.regions.RedisNaturalIdRegion;
import com.bc.plugin.redis.hibernate.regions.RedisQueryResultsRegion;
import com.bc.plugin.redis.hibernate.regions.RedisTimestampsRegion;

public abstract class AbstractRedisRegionFactory implements RegionFactory {

	protected SessionFactoryOptions options;

	@Override
	public boolean isMinimalPutsEnabledByDefault() {
		return true;
	}

	@Override
	public AccessType getDefaultAccessType() {
		return AccessType.READ_WRITE;
	}

	@Override
	public EntityRegion buildEntityRegion(String regionName, Properties properties, CacheDataDescription metadata)
			throws CacheException {
		return new RedisEntityRegion(options, metadata, regionName, properties);
	}

	@Override
	public NaturalIdRegion buildNaturalIdRegion(String regionName, Properties properties, CacheDataDescription metadata)
			throws CacheException {
		return new RedisNaturalIdRegion(options, metadata, regionName, properties);
	}

	@Override
	public CollectionRegion buildCollectionRegion(String regionName, Properties properties,
			CacheDataDescription metadata) throws CacheException {
		return new RedisCollectionRegion(options, metadata, regionName, properties);
	}

	@Override
	public QueryResultsRegion buildQueryResultsRegion(String regionName, Properties properties) throws CacheException {
		return new RedisQueryResultsRegion(regionName, properties);
	}

	@Override
	public TimestampsRegion buildTimestampsRegion(String regionName, Properties properties) throws CacheException {
		return new RedisTimestampsRegion(regionName, properties);
	}

}
