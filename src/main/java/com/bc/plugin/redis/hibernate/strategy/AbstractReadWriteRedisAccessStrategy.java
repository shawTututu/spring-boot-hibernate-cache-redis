package com.bc.plugin.redis.hibernate.strategy;

import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.engine.spi.SharedSessionContractImplementor;

import com.bc.plugin.redis.hibernate.regions.RedisTransactionalDataRegion;

public abstract class AbstractReadWriteRedisAccessStrategy<T extends RedisTransactionalDataRegion>
		extends AbstractRedisAccessStrategy<T> {

	//private final Comparator<?> versionComparator;

	public AbstractReadWriteRedisAccessStrategy(T region, SessionFactoryOptions options) {
		super(region, options);
		//this.versionComparator = region.getCacheDataDescription().getVersionComparator();
	}

	@Override
	public boolean putFromLoad(SharedSessionContractImplementor session, Object key, Object value, long txTimestamp,
			Object version, boolean minimalPutOverride) {
		region.put(key, value);
		return true;
	}

}
