package com.bc.plugin.redis.hibernate.regions;

import java.util.Properties;

import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.CacheDataDescription;
import org.hibernate.cache.spi.CollectionRegion;
import org.hibernate.cache.spi.access.AccessType;
import org.hibernate.cache.spi.access.CollectionRegionAccessStrategy;

import com.bc.plugin.redis.hibernate.strategy.ReadWriteRedisCollectionRegionAccessStrategy;


public class RedisCollectionRegion extends RedisTransactionalDataRegion implements CollectionRegion {

	public RedisCollectionRegion(SessionFactoryOptions options, CacheDataDescription metadata, String regionName,
			Properties props) {
		super(options, metadata, regionName, props);
	}

	@Override
	public CollectionRegionAccessStrategy buildAccessStrategy(AccessType accessType) throws CacheException {
		return new ReadWriteRedisCollectionRegionAccessStrategy(this, options);
	}

}
