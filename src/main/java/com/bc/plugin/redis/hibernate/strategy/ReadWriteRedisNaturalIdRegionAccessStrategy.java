
package com.bc.plugin.redis.hibernate.strategy;

import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.internal.DefaultCacheKeysFactory;
import org.hibernate.cache.spi.NaturalIdRegion;
import org.hibernate.cache.spi.access.NaturalIdRegionAccessStrategy;
import org.hibernate.cache.spi.access.SoftLock;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.persister.entity.EntityPersister;

import com.bc.plugin.redis.hibernate.regions.RedisNaturalIdRegion;

public class ReadWriteRedisNaturalIdRegionAccessStrategy
		extends AbstractReadWriteRedisAccessStrategy<RedisNaturalIdRegion> implements NaturalIdRegionAccessStrategy {

	public ReadWriteRedisNaturalIdRegionAccessStrategy(RedisNaturalIdRegion region, SessionFactoryOptions options) {
		super(region, options);
	}

	@Override
	public Object generateCacheKey(Object[] naturalIdValues, EntityPersister persister,
			SharedSessionContractImplementor session) {
		return DefaultCacheKeysFactory.INSTANCE.createNaturalIdKey(naturalIdValues, persister, session);
	}

	@Override
	public Object[] getNaturalIdValues(Object cacheKey) {
		return DefaultCacheKeysFactory.INSTANCE.getNaturalIdValues(cacheKey);
	}

	@Override
	public NaturalIdRegion getRegion() {
		return region;
	}

	@Override
	public boolean insert(SharedSessionContractImplementor session, Object key, Object value) {
		region.put(key, value);
		return true;
	}

	@Override
	public boolean afterInsert(SharedSessionContractImplementor session, Object key, Object value) {
		region.put(key, value);
		return true;
	}

	@Override
	public boolean update(SharedSessionContractImplementor session, Object key, Object value) {
		region.put(key, value);
		return true;
	}

	@Override
	public boolean afterUpdate(SharedSessionContractImplementor session, Object key, Object value, SoftLock lock) {
		region.put(key, value);
		return true;
	}

}
