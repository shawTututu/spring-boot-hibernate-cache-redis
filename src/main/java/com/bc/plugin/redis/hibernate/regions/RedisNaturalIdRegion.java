package com.bc.plugin.redis.hibernate.regions;

import java.util.Properties;

import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.CacheDataDescription;
import org.hibernate.cache.spi.NaturalIdRegion;
import org.hibernate.cache.spi.access.AccessType;
import org.hibernate.cache.spi.access.NaturalIdRegionAccessStrategy;

import com.bc.plugin.redis.hibernate.strategy.ReadWriteRedisNaturalIdRegionAccessStrategy;

public class RedisNaturalIdRegion extends RedisTransactionalDataRegion implements NaturalIdRegion {
	public RedisNaturalIdRegion(SessionFactoryOptions options, CacheDataDescription metadata, String regionName,
			Properties props) {
		super(options, metadata, regionName, props);
		// TODO Auto-generated constructor stub
	}

	@Override
	public NaturalIdRegionAccessStrategy buildAccessStrategy(AccessType accessType) throws CacheException {
		return new ReadWriteRedisNaturalIdRegionAccessStrategy(this, options);
	}

}
