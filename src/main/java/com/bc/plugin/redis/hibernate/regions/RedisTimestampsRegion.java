package com.bc.plugin.redis.hibernate.regions;

import java.util.Properties;

import org.hibernate.cache.spi.TimestampsRegion;

public class RedisTimestampsRegion extends RedisGeneralDataRegion implements TimestampsRegion {
	public RedisTimestampsRegion(String regionName, Properties props) {
		super(regionName, props);
	}

}
