package com.bc.plugin.redis.hibernate.regions;

import java.util.Properties;

import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.CacheDataDescription;
import org.hibernate.cache.spi.EntityRegion;
import org.hibernate.cache.spi.access.AccessType;
import org.hibernate.cache.spi.access.EntityRegionAccessStrategy;

import com.bc.plugin.redis.hibernate.strategy.ReadWriteRedisEntityRegionAccessStrategy;

public class RedisEntityRegion extends RedisTransactionalDataRegion implements EntityRegion {

	public RedisEntityRegion(SessionFactoryOptions options, CacheDataDescription metadata, String regionName,
			Properties props) {
		super(options, metadata, regionName, props);
		// TODO Auto-generated constructor stub
	}

	@Override
	public EntityRegionAccessStrategy buildAccessStrategy(AccessType accessType) throws CacheException {
		return new ReadWriteRedisEntityRegionAccessStrategy(this, options);
	}

}
