package com.bc.plugin.redis.hibernate.regions;

import java.util.Properties;

import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.spi.CacheDataDescription;
import org.hibernate.cache.spi.TransactionalDataRegion;

import lombok.Getter;

@Getter
public class RedisTransactionalDataRegion extends RedisDataRegion implements TransactionalDataRegion {

	public RedisTransactionalDataRegion(SessionFactoryOptions options, CacheDataDescription metadata, String regionName,
			Properties props) {
		super(regionName, props);
		this.options = options;
		this.metadata = metadata;
	}

	protected final SessionFactoryOptions options;
	
	protected final CacheDataDescription metadata;

	@Override
	public boolean isTransactionAware() {
		return false;
	}

	@Override
	public CacheDataDescription getCacheDataDescription() {
		return metadata;
	}

}
