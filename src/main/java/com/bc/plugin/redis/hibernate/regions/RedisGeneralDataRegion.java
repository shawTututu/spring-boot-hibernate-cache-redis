package com.bc.plugin.redis.hibernate.regions;

import java.util.Properties;

import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.GeneralDataRegion;
import org.hibernate.engine.spi.SharedSessionContractImplementor;

public class RedisGeneralDataRegion extends RedisDataRegion implements GeneralDataRegion {

	public RedisGeneralDataRegion(String regionName, Properties props) {
		super(regionName, props);
	}

	@Override
	public Object get(SharedSessionContractImplementor session, Object key) throws CacheException {
		return this.get(key);
	}

	@Override
	public void put(SharedSessionContractImplementor session, Object key, Object value) throws CacheException {
		this.put(key, value);
	}

	@Override
	public void evict(Object key) throws CacheException {
		this.remove(key);
	}

	@Override
	public void evictAll() throws CacheException {
		this.clear();
	}
}
