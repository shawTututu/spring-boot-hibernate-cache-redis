
package com.bc.plugin.redis.hibernate.regions;

import java.util.Properties;

import org.hibernate.cache.spi.QueryResultsRegion;

public class RedisQueryResultsRegion extends RedisGeneralDataRegion implements QueryResultsRegion {
	public RedisQueryResultsRegion(String regionName, Properties props) {
		super(regionName, props);
	}

}
