# spring-boot-hibernate-cache-redis

#### 介绍
hibernate二级缓存的redis实现，第一个版本只有简单的实现，基本可用，未实现锁功能
替换ehcache二级缓存，缓存数据保存在redis中，其他使用方式不变

ehcache二级缓存方案： https://my.oschina.net/u/1428688/blog/2962103

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

hibernate二级缓存生效步骤：

1、queryImpl.setCacheable(true);必须设置才会生效
private void setHibernateQuery(Query query, Class<?> modelClass) {
		if (modelClass.isAnnotationPresent(Cacheable.class)) {
			if (query instanceof QueryImpl) {
				QueryImpl<?> queryImpl = (QueryImpl<?>) query;
				queryImpl.setCacheable(true);
			}
		}
}

2、在实体上增加@cacheable（javax.persistence.Cacheable）


3、spring boot 2.x 原生配置

spring.jpa.properties.javax.persistence.sharedCache.mode = ENABLE_SELECTIVE
spring.jpa.properties.hibernate.cache.use_query_cache = true
spring.jpa.properties.hibernate.cache.use_second_level_cache = true
spring.jpa.properties.hibernate.cache.region.factory_class = com.bc.plugin.redis.hibernate.RedisRegionFactory

spring.redis.host = localhost
spring.redis.password =
spring.redis.database = 

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)